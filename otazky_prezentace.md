# Otázky na prezentaci

**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka | vyjádření |
| :--- | :--- |
| jak dlouho mi tvorba zabrala - **čistý čas** | 2 h plánování + 6 h pájení + 2 h programování + 2 h video |
| jak se mi to podařilo rozplánovat | dobře, vše jsem stihl |
| design zapojení | https://gitlab.spseplzen.cz/turhoberv/vlastni-shield/-/blob/main/dokumentace/schema/fritzing_zapojeni.png |
| proč jsem zvolil tento design | jednoduchost |
| zapojení | https://gitlab.spseplzen.cz/turhoberv/vlastni-shield/-/blob/main/dokumentace/schema/fritzing_zapojeni.png |
| z jakých součástí se zapojení skládá | Wemos R1 D1, Arduino proto shield v.5, LCD Displej, LED pásek, teplotní čislo, rezistor 220 Ohm, 4.7 kOhm, 10 kOhm, LEDky, fotorezistor, kabely |
| realizace | https://gitlab.spseplzen.cz/turhoberv/vlastni-shield/-/blob/main/dokumentace/design/20240929_150232.jpg |
| nápad, v jakém produktu vše propojit dohromady| Teploměr s LED páskem |
| co se mi povedlo | funguje to |
| co se mi nepovedlo/příště bych udělal/a jinak | pro estetiku jsem ustříhl teplotní čidlo (prý to není ideální) |
| zhodnocení celé tvorby | 9/10 |
