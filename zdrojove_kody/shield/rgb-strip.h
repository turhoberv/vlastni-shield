// připojení potřebné knihovny
#include <Adafruit_NeoPixel.h>


// nastavení propojovacího pinu
#define pinDIN D6
// nastavení počtu LED modulů
#define pocetLED 8
// inicializace LED modulu z knihovny
Adafruit_NeoPixel rgbWS = Adafruit_NeoPixel(pocetLED, pinDIN, NEO_GRB + NEO_KHZ800);

byte divider = 5;
byte lastRed, lastGreen, lastBlue;

void setDivider(byte x){
  divider = x;
}

// funkce pro nastavení zadané barvy na zvolenou diodu

void nastavRGB(byte r, byte g, byte b, int cislo) {
  // vytvoření proměnné pro ukládání barev
  uint32_t barva;
  // načtení barvy do proměnné
  barva = rgbWS.Color(r, g, b);
  // nastavení barvy pro danou LED diodu,
  // číslo má pořadí od nuly
  rgbWS.setPixelColor(cislo - 1, barva);
  // aktualizace barev na všech modulech
  rgbWS.show();
}

void setAll(byte r, byte g, byte b) {
  for (int i = 0; i < 8; i++) {
    nastavRGB(r, g, b, i + 1);
  }
}

void chaos() {
  // pomocí funkce nastavRGB vytvoř náhodnou kombinaci barev
  // a pro každý běh smyčky vyber náhodnou LED diodu,
  // funkce random vytvoří náhodné číslo
  // z rozsahu v závorkách random(min, max-1)
  byte max = 255;
  byte cervena = random(0, max);
  byte zelena = random(0, max);
  byte modra = random(0, max);
  byte dioda = random(1, (pocetLED + 1));
  nastavRGB(cervena, zelena, modra, dioda);
}
void betterChaos() {
  // pomocí funkce nastavRGB vytvoř náhodnou kombinaci barev
  // a pro každý běh smyčky vyber náhodnou LED diodu,
  // funkce random vytvoří náhodné číslo
  // z rozsahu v závorkách random(min, max-1)
  byte cervena, zelena, modra;
  byte max = 255;
  byte min_diff = 240;
  do {
    cervena = random(0, max);
    zelena = random(0, max);
    modra = random(0, max);
  } while (((zelena - cervena) < min_diff && (cervena - zelena) < min_diff) && ((zelena - modra) < min_diff && (modra - zelena) < min_diff) && ((cervena - modra) < min_diff && (modra - cervena) < min_diff));
  byte dioda = random(1, (pocetLED + 1));
  nastavRGB(cervena, zelena, modra, dioda);
}

void betterChaosAll() {
  // pomocí funkce nastavRGB vytvoř náhodnou kombinaci barev
  // a pro každý běh smyčky vyber náhodnou LED diodu,
  // funkce random vytvoří náhodné číslo
  // z rozsahu v závorkách random(min, max-1)
  byte cervena, zelena, modra;
  byte max = 255;
  byte min_diff = 240;
  do {
    cervena = random(0, max);
    zelena = random(0, max);
    modra = random(0, max);
  } while ((((zelena - cervena) < min_diff && (cervena - zelena) < min_diff) && ((zelena - modra) < min_diff && (modra - zelena) < min_diff) && ((cervena - modra) < min_diff && (modra - cervena) < min_diff))||((lastGreen - zelena <100 && zelena - lastGreen < 100)&&(lastBlue - modra <100 && modra - lastBlue < 100)&&(lastRed - cervena <100 && cervena - lastRed < 100)));
  byte dioda = random(1, (pocetLED + 1));
    lastRed = cervena;
    lastGreen = zelena;
    lastBlue = modra;
  setAll(cervena, zelena, modra);
}

